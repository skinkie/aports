# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=plasma-thunderbolt
pkgver=5.27.2
pkgrel=0
# armhf blocked by qt5-qtdeclarative
arch="all !armhf"
url="https://kde.org/plasma-desktop/"
pkgdesc="Plasma integration for controlling Thunderbolt devices"
license="GPL-2.0-only OR GPL-3.0-only"
depends="bolt"
makedepends="
	extra-cmake-modules
	kcmutils-dev
	kcoreaddons-dev
	kdbusaddons-dev
	kdeclarative-dev
	ki18n-dev
	knotifications-dev
	qt5-qtbase-dev
	qt5-qtdeclarative-dev
	samurai
	"
checkdepends="xvfb-run"

case "$pkgver" in
	*.90*) _rel=unstable;;
	*) _rel=stable;;
esac
source="https://download.kde.org/$_rel/plasma/$pkgver/plasma-thunderbolt-$pkgver.tar.xz"
subpackages="$pkgname-lang"
options="!check" # Requires running dbus server

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

check() {
	cd build
	CTEST_OUTPUT_ON_FAILURE=TRUE xvfb-run ctest
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
638b6d85daf4528e624a6421b5bf63da586a870626f176df5a1308441fbeceac79d497ba366522074db41d1906b28465b1d9ed4aa2b0a8ac0ad9d7b8d4faa288  plasma-thunderbolt-5.27.2.tar.xz
"
