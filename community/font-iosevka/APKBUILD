# Contributor: psykose <alice@ayaya.dev>
# Maintainer: psykose <alice@ayaya.dev>
pkgname=font-iosevka
pkgver=20.0.0
pkgrel=2
pkgdesc="Versatile typeface for code, from code"
url="https://typeof.net/Iosevka/"
arch="noarch"
options="!check" # no testsuite
license="OFL-1.1"
depends="fontconfig"
subpackages="
	$pkgname-base
	$pkgname-aile
	$pkgname-slab
	$pkgname-curly
	$pkgname-curly-slab:curly_slab
	"
source="
	https://github.com/be5invis/Iosevka/releases/download/v$pkgver/super-ttc-iosevka-$pkgver.zip
	https://github.com/be5invis/Iosevka/releases/download/v$pkgver/super-ttc-iosevka-aile-$pkgver.zip
	https://github.com/be5invis/Iosevka/releases/download/v$pkgver/super-ttc-iosevka-slab-$pkgver.zip
	https://github.com/be5invis/Iosevka/releases/download/v$pkgver/super-ttc-iosevka-curly-$pkgver.zip
	https://github.com/be5invis/Iosevka/releases/download/v$pkgver/super-ttc-iosevka-curly-slab-$pkgver.zip
	"

builddir="$srcdir"

package() {
	depends="
		$pkgname-base=$pkgver-r$pkgrel
		$pkgname-aile=$pkgver-r$pkgrel
		$pkgname-slab=$pkgver-r$pkgrel
		$pkgname-curly=$pkgver-r$pkgrel
		$pkgname-curly-slab=$pkgver-r$pkgrel
		"

	install -Dm644 "$builddir"/*.ttc \
		-t "$pkgdir"/usr/share/fonts/${pkgname#font-}
}

base() {
	pkgdesc="$pkgdesc (Iosevka)"
	amove usr/share/fonts/iosevka/iosevka.ttc
}

aile() {
	pkgdesc="$pkgdesc (Iosevka Aile)"
	amove usr/share/fonts/iosevka/iosevka-aile.ttc
}

slab() {
	pkgdesc="$pkgdesc (Iosevka Slab)"
	amove usr/share/fonts/iosevka/iosevka-slab.ttc
}

curly() {
	pkgdesc="$pkgdesc (Iosevka Curly)"
	amove usr/share/fonts/iosevka/iosevka-curly.ttc
}

curly_slab() {
	pkgdesc="$pkgdesc (Iosevka Curly Slab)"
	amove usr/share/fonts/iosevka/iosevka-curly-slab.ttc
}

sha512sums="
f2839a3e0249a921b2f25f7c0c01086bffe547ceb04c4e7137d34a41431b1ee4a205de830709fd5359c6210e7ecbcb3326331c05471fae7f408097c98c62bc91  super-ttc-iosevka-20.0.0.zip
2fed845470ea576cad89b183fa9faa189469ca85b123c80d98a4181d31b21d1771fc6a433428ad84f4bda4d8da8564799d42e16fe6ad31a3b524d7d069833471  super-ttc-iosevka-aile-20.0.0.zip
17ad812f3afc11de6b8fd644bda6d6675bd339b47f7b65f51815058213f0fd418af8eea7ed10c7e19788fa5931e54d57ea8ab246f5583b2e127fd2bc036a80f0  super-ttc-iosevka-slab-20.0.0.zip
ecbb25d2b3b6e875136a18f303e198b8372fedbd94259ed09294136b17f2c12cf241e769531faa6588f2e637820a6f715b7e6f9442dca817584e9e52e0587799  super-ttc-iosevka-curly-20.0.0.zip
25eb9bc63b01a0e615620aa8b7b457295da589cac95fc589fdd5317d0949c48aeaded7069d505833a65bf2edc22e983c5d4b7f676fb5af8674f9f4c07af858fa  super-ttc-iosevka-curly-slab-20.0.0.zip
"
