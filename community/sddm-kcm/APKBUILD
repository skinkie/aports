# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=sddm-kcm
pkgver=5.27.2
pkgrel=0
pkgdesc="Config module for SDDM"
# armhf blocked by extra-cmake-modules
arch="all !armhf"
url="https://www.kde.org"
license="GPL-2.0-or-later AND (GPL-2.0-only OR GPL-3.0-only) AND LGPL-2.1-or-later AND GPL-2.0-only"
depends="
	sddm
	systemsettings
	"
makedepends="
	extra-cmake-modules
	karchive-dev
	kauth-dev
	kcmutils-dev
	kconfigwidgets-dev
	kcoreaddons-dev
	kdeclarative-dev
	ki18n-dev
	kio-dev
	knewstuff-dev
	kxmlgui-dev
	libxcursor-dev
	qt5-qtbase-dev
	qt5-qtdeclarative-dev
	qt5-qtx11extras-dev
	samurai
	xcb-util-image-dev
	"

case "$pkgver" in
	*.90*) _rel=unstable;;
	*) _rel=stable;;
esac
source="https://download.kde.org/$_rel/plasma/$pkgver/sddm-kcm-$pkgver.tar.xz"
subpackages="$pkgname-lang"
options="!check" # No tests

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}
sha512sums="
c0a5a1c79fd47aec289b71c56c063854eaa4dee5c1338be3924ddb3147b4e9132928d32860c76b7c22730bd057b240d305135d49d96807adbabeb5195bc29367  sddm-kcm-5.27.2.tar.xz
"
